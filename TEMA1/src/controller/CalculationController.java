package controller;

import java.util.ArrayList;

import data.Monom;
import data.Polinom;

public class CalculationController {
	private Polinom polinomOne = new Polinom();
	private Polinom polinomTwo = new Polinom();

	private Polinom additionPolinom = new Polinom();
	private Polinom subtractionPolinom = new Polinom();

	public String addPolinomOne(int coefficient, int power) {
		polinomOne.setValues(polinomOne.addMonom(new Monom(coefficient, power)));
		return polinomOne.toString();
	}

	public String addPolinomTwo(int coefficient, int power) {
		polinomTwo.setValues(polinomTwo.addMonom(new Monom(coefficient, power)));
		return polinomTwo.toString();
	}

	public String doAddition() {
		Polinom firstTerm = new Polinom();
		Polinom secondTerm = new Polinom();
		firstTerm.cloneOf(polinomOne);
		secondTerm.cloneOf(polinomTwo);
		additionPolinom.setValues(firstTerm.getValues());
		for (Monom monom : secondTerm.getValues()) {
			additionPolinom.setValues(additionPolinom.addMonom(monom));
		}
		return additionPolinom.toString();
	}

	public String doSubtraction() {
		Polinom firstTerm = new Polinom();
		Polinom secondTerm = new Polinom();
		firstTerm.cloneOf(polinomOne);
		secondTerm.cloneOf(polinomTwo);
		subtractionPolinom.setValues(firstTerm.getValues());
		for (Monom monom : secondTerm.getValues()) {
			subtractionPolinom.setValues(new ArrayList<Monom>(subtractionPolinom.subtractMonom(monom)));
		}
		return subtractionPolinom.toString();
	}

	/**
	 * multiply two polinoms
	 * 
	 * @return
	 */
	public String doMultiplication() {
		Polinom firstTerm = new Polinom();
		Polinom secondTerm = new Polinom();
		firstTerm.cloneOf(polinomOne);
		secondTerm.cloneOf(polinomTwo);
		Polinom multimplicationPolinom = new Polinom();
		for (Monom monom1 : firstTerm.getValues()) {
			for (Monom monom2 : secondTerm.getValues()) {
				multimplicationPolinom.setValues(multimplicationPolinom.addMonom(multiplyMonoms(monom1, monom2)));
			}
		}
		return multimplicationPolinom.toString();
	}

	/**
	 * Multiply two monoms. Used in polynom multiplication
	 * 
	 * @param monom1
	 * @param monom2
	 * @return
	 */
	public Monom multiplyMonoms(Monom monom1, Monom monom2) {
		Monom monom = new Monom();
		monom.setCoefficient(monom1.getCoefficient() * monom2.getCoefficient());
		monom.setPower(monom1.getPower() + monom2.getPower());
		return monom;
	}

	/**
	 * Derivate a polinom
	 * 
	 * @return polynom's value as a string
	 */
	public String doDerivate() {
		Polinom firstTerm = new Polinom();
		firstTerm.cloneOf(polinomOne);
		for (Monom monom : firstTerm.getValues()) {
			monom.setCoefficient(monom.getCoefficient() * monom.getPower());
			monom.setPower(monom.getPower() - 1);
		}
		return firstTerm.toString();
	}

	public void clearPolinomOone() {
		polinomOne = new Polinom();
	}

	public void clearPolinomTwo() {
		polinomTwo = new Polinom();
	}
}