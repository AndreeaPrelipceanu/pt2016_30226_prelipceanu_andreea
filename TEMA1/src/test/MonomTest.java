package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import data.Monom;

public class MonomTest {
	private Monom monom;

	@Before
	public void setUp() {
		monom = new Monom(2, 3);
	}

	@Test
	public void toStringTest() {

		String result = monom.toString();

		assertEquals("+2X^3", result);
	}
}
