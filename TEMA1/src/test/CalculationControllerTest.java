package test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import controller.CalculationController;
import data.Monom;

public class CalculationControllerTest {

	private CalculationController controller;

	@Before
	public void setUp() {
		controller = new CalculationController();
	}

	@Test
	public void multipyMonomsTest() {
		Monom monom1 = new Monom(2, 3);
		Monom monom2 = new Monom(2, 5);
		Monom result = controller.multiplyMonoms(monom1, monom2);
		assertEquals(4, result.getCoefficient());
		assertEquals(8, result.getPower());

	}

}
