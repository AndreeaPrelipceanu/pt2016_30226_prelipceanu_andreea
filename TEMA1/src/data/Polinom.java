package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Polinom {
	private List<Monom> values = new ArrayList<Monom>();

	@Override
	public String toString() {
		String valueAsString = "";
		for (Monom monom : this.values) {
			valueAsString = valueAsString + monom.toString();
		}
		return valueAsString;
	}

	public List<Monom> addMonom(Monom monom) {
		List<Monom> addedValues = this.values;
		addMonomToValueList(monom, addedValues);
		return addedValues;
	}

	public List<Monom> subtractMonom(Monom monom) {
		monom.setCoefficient(monom.getCoefficient() * (-1));
		List<Monom> addedValues = new ArrayList<Monom>(this.values);
		addMonomToValueList(monom, addedValues);
		return addedValues;
	}

	private void addMonomToValueList(Monom monom, List<Monom> addedValues) {
		boolean existingPower = false;

		if (addedValues.isEmpty()) {
			addedValues.add(monom);
		} else {
			for (Monom currentMonom : addedValues) {
				if (monom.getPower() == currentMonom.getPower()) {
					currentMonom.setCoefficient(currentMonom.getCoefficient() + monom.getCoefficient());
					existingPower = true;
					break;
				}
			}
			if (!existingPower) {
				addedValues.add(monom);
			}
			sortValues(addedValues);
		}
	}

	private void sortValues(List<Monom> values) {
		Collections.sort(values, new Comparator<Monom>() {
			public int compare(Monom m1, Monom m2) {
				return m2.getPower() - m1.getPower();
			}
		});
	}

	private List<Monom> cloneValues(List<Monom> source) {
		this.values = new ArrayList<Monom>();
		for (Monom monom : source) {
			this.values.add(new Monom().cloneOf(monom));
		}

		return values;
	}

	public Polinom cloneOf(Polinom source) {
		cloneValues(source.getValues());
		return this;
	}

	public List<Monom> getValues() {
		return values;
	}

	public void setValues(List<Monom> values) {
		this.values = values;
	}
}
