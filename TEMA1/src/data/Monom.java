package data;

public class Monom {

	private int coefficient;
	private int power;

	public Monom() {
	};

	public Monom(int coefficient, int power) {
		this.coefficient = coefficient;
		this.power = power;
	}

	@Override
	public String toString() {
		String sign = "";
		if (coefficient >= 0)
			sign = "+";
		return sign + coefficient + "X^" + power; // suprascrierea tostring pt
													// afisarea
		// mon.
	}

	public int getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public Monom cloneOf(Monom sourceMonom) {
		this.power = sourceMonom.getPower();
		this.coefficient = sourceMonom.getCoefficient();
		return this;
	}
}
