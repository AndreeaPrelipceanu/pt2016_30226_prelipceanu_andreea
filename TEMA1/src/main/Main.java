package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.CalculationController;

public class Main {

	public static void main(String[] args) {
		// Creating instance of JFrame
		JFrame frame = new JFrame("My First Swing Example");
		// Setting the width and height of frame
		frame.setSize(800, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		/*
		 * Creating panel. This is same as a div tag in HTML We can create
		 * several panels and add them to specific positions in a JFrame. Inside
		 * panels we can add text fields, buttons and other components.
		 */
		JPanel panel = new JPanel();
		// adding panel to frame
		frame.add(panel);
		/*
		 * calling user defined method for adding components to the panel.
		 */
		CalculationController controller = new CalculationController();
		placeComponents(panel, controller);

		// Setting the frame visibility to true
		frame.setVisible(true);
	}

	private static void placeComponents(JPanel panel, CalculationController controller) {

		/*
		 * We will discuss about layouts in the later sections of this tutorial.
		 * For now we are setting the layout to null
		 */
		panel.setLayout(null);

		// Creating JLabel
		JLabel polinomOneLabel = new JLabel("Polinom 1");
		/*
		 * This method specifies the location and size of component.
		 * setBounds(x, y, width, height) here (x,y) are cordinates from the top
		 * left corner and remaining two arguments are the width and height of
		 * the component.
		 */
		polinomOneLabel.setBounds(10, 20, 80, 25);
		panel.add(polinomOneLabel);

		/*
		 * Creating text field where user is supposed to enter user name.
		 */
		JTextField polinomOneText = new JTextField(20);
		polinomOneText.setBounds(100, 20, 180, 25);
		panel.add(polinomOneText);

		JLabel coefOneLabel = new JLabel("Coef");
		coefOneLabel.setBounds(290, 20, 80, 25);
		panel.add(coefOneLabel);

		JTextField coefOneText = new JTextField(20);
		coefOneText.setBounds(320, 20, 30, 25);
		panel.add(coefOneText);

		JLabel powOneLabel = new JLabel("Pow");
		powOneLabel.setBounds(350, 20, 80, 25);
		panel.add(powOneLabel);

		JTextField powOneText = new JTextField(20);
		powOneText.setBounds(380, 20, 30, 25);
		panel.add(powOneText);

		// Creating add2 button
		JButton addOneButton = new JButton("Add");
		addOneButton.setBounds(420, 20, 70, 25);
		panel.add(addOneButton);

		// Creating clear button
		JButton clearOneButton = new JButton("Clear");
		clearOneButton.setBounds(500, 20, 70, 25);
		panel.add(clearOneButton);

		// Same process for password label and text field.
		JLabel polinomTwoLabel = new JLabel("Polinom 2");
		polinomTwoLabel.setBounds(10, 50, 80, 25);
		panel.add(polinomTwoLabel);

		/*
		 * This is similar to text field but it hides the user entered data and
		 * displays dots instead to protect the password like we normally see on
		 * login screens.
		 */
		JTextField polinomTwoText = new JTextField(20);
		polinomTwoText.setBounds(100, 50, 180, 25);
		panel.add(polinomTwoText);

		JLabel coefTwoLabel = new JLabel("Coef");
		coefTwoLabel.setBounds(290, 50, 80, 25);
		panel.add(coefTwoLabel);

		JTextField coefTwoText = new JTextField(20);
		coefTwoText.setBounds(320, 50, 30, 25);
		panel.add(coefTwoText);

		JLabel powTwoLabel = new JLabel("Pow");
		powTwoLabel.setBounds(350, 50, 80, 25);
		panel.add(powTwoLabel);

		JTextField powTwoText = new JTextField(20);
		powTwoText.setBounds(380, 50, 30, 25);
		panel.add(powTwoText);
		// Creating add2 button
		JButton addTwoButton = new JButton("Add");
		addTwoButton.setBounds(420, 50, 70, 25);
		panel.add(addTwoButton);

		// Creating clear2 button
		JButton clearTwoButton = new JButton("Clear");
		clearTwoButton.setBounds(500, 50, 70, 25);
		panel.add(clearTwoButton);

		// Creating addition button
		JButton additionButton = new JButton("+");
		additionButton.setBounds(10, 100, 70, 25);
		panel.add(additionButton);

		JTextField additionText = new JTextField(20);
		additionText.setBounds(100, 100, 180, 25);
		panel.add(additionText);

		// Creating subtraction button
		JButton subtractionButton = new JButton("-");
		subtractionButton.setBounds(10, 130, 70, 25);
		panel.add(subtractionButton);

		JTextField subtractionText = new JTextField(20);
		subtractionText.setBounds(100, 130, 180, 25);
		panel.add(subtractionText);

		// Creating multiplication button
		JButton multiplicationButton = new JButton("*");
		multiplicationButton.setBounds(10, 160, 70, 25);
		panel.add(multiplicationButton);

		JTextField multiplicationText = new JTextField(20);
		multiplicationText.setBounds(100, 160, 180, 25);
		panel.add(multiplicationText);

		// Creating division button
		JButton divisionButton = new JButton("/");
		divisionButton.setBounds(10, 190, 70, 25);
		panel.add(divisionButton);

		JTextField divisionText = new JTextField(20);
		divisionText.setBounds(100, 190, 180, 25);
		panel.add(divisionText);

		// Creating derivative button
		JButton derivativeButton = new JButton("(=)'");
		derivativeButton.setBounds(10, 220, 70, 25);
		panel.add(derivativeButton);

		JTextField derivativeText = new JTextField(20);
		derivativeText.setBounds(100, 220, 180, 25);
		panel.add(derivativeText);

		// Creating integral button
		JButton integralButton = new JButton("._/' ");
		integralButton.setBounds(10, 250, 70, 25);
		panel.add(integralButton);

		JTextField integralText = new JTextField(20);
		integralText.setBounds(100, 250, 180, 25);
		panel.add(integralText);

		addOneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				polinomOneText.setText(controller.addPolinomOne(Integer.parseInt(coefOneText.getText()),
						Integer.parseInt(powOneText.getText())));
			}
		});

		addTwoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				polinomTwoText.setText(controller.addPolinomTwo(Integer.parseInt(coefTwoText.getText()),
						Integer.parseInt(powTwoText.getText())));
			}
		});

		clearOneButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				polinomOneText.setText("");
				controller.clearPolinomOone();
			}
		});

		clearTwoButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				polinomTwoText.setText("");
				controller.clearPolinomTwo();
			}
		});

		additionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				additionText.setText(controller.doAddition());

			}
		});

		subtractionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				subtractionText.setText(controller.doSubtraction());

			}
		});

		multiplicationButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				multiplicationText.setText(controller.doMultiplication());
			}
		});

		derivativeButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				derivativeText.setText(controller.doDerivate());
			}
		});

	}

}
