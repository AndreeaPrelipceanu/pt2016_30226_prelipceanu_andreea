package t3;
import java.util.*;

/**
 * Clasa Client 
 * retine datele despre client
 */           
public class Client
{
    private long cID; 
    private Date tsosire; 
    private Date tasteptare;

/**
 * Constructorul Client 
 * retine cID, tsosire, tservire al fiecarui client
 */           
public Client( long cID, Date tsosire, Date tasteptare)
    { this.cID = cID; 
      this.tsosire = tsosire; 
      this.tasteptare=tasteptare;
    }

/**
 * Metoda getCID 
 * returneaza id-ul clientului 
 */           
    public long getCID(Client C)
        {
            return C.cID;   
        }

/**
 * Metoda getTsosire 
 * returneaza timpul de sosire al clientului
 */                   
    public Date getTsosire()
    { 
        return tsosire; 
    }
            
/**
 * Metoda getTservire 
 * returneaza timpul de servire al clientului
 */  
    
    public Date getTasteptare(Client C)
    {
        return C.tasteptare;
    }
    
 /**
 * Metoda toString
 * returneaza un string cu datele clientului
 */               
    public String toString()
    { 
        return ( Long.toString(cID)+" "+tsosire.toString()+" "+tasteptare.toString());
    }

                  
}