package t3;  

import java.util.*;
import java.text.SimpleDateFormat;

          
public class Producator extends Thread
{
    private Casa casa[]; //se creeaza un vector de obiecte de tip casa
    private int nr_case; 
    int [] stat= new int[nr_case];
    int [] nrstat= new int[nr_case];
    static long ID =0; 
    private int nr_clienti;
    public int minT;
    public int maxT;
    private StringBuilder logString= new StringBuilder();

    public int min;
    private static int timp;//folosit pentru gestionararea timpului
    public SetNumber number=new SetNumber();
    public static int tSosire, tservire;
    private static int ore, minute, secunde;
    public int max_clienti=5;
/**
 * Constructorul Producator 
 * creaza fiecare casa
 */           
public Producator( int nr_case, Casa casa[], String name, int nr_clienti )
    { 
      setName( name ); 
      this.nr_case = nr_case;
      this.casa = new Casa[ nr_case+1];
      this.nr_clienti = nr_clienti;
        for( int i=1; i<=nr_case; i++)
            { 
                this.casa[ i ] =casa[ i ] ;
            } 
    }

 /**
 * Metoda min_index 
 * calculeaza indlxul casei cu cei mai putini clienti 
 */               
private int min_index ()
{ 
  int index = 1; 
  try 
    { 
      long min = casa[1].lungime_coada(); //se retine initial casa 1 ca avand cel mai mic numar de clienti
      for( int i=2; i<=nr_case; i++)
        { 
          long lung = casa[ i ].lungime_coada(); 
          if ( lung < min ) //daca se gaseste alta casa mai goala
            { 
              min = lung; //se retine aceasta
              index = i;
            } 
        } 
    } 
  catch( InterruptedException e )
    { System.out.println( e.toString()); } 
  return index;
}


public void convert(int x)    //se ia ca argument valoarea generata aleator
	{
	secunde=x%60;
	minute=x/60;
	ore=10+(x/3600)%60;
	}

/**
 * Metoda run 
 * creaza fiecare client
 * il adauga in casa
 * si il pune sa astepte
 */           
public void run()
{ 
try 
    { 
      int i=0; 
      int med=0;
      while( i<nr_clienti ){ //contorul i parcurge clientii
          i++; 
          tSosire=number.Timp(10);//se genereaza un timp aleatoriu de sosire
          tservire=number.Timp(maxT-minT);
          int m = min_index(); //se retine casa cu numarul minim de clienti
          Client c = new Client( ++ID, new Date(), new Date()); //se creeaza unu nou client cu aceste caracteristici
          timp = timp+ tSosire;
          convert(timp);
          if ((minute<10)&&(secunde<10))
		GUI.rezultat.append("Clientul "+Long.toString( ID )+" a sosit la "+ore+":0"+minute+":0"+secunde+". ");
			else if ((minute<10)&&(secunde>=10))
		GUI.rezultat.append("Clientul "+Long.toString( ID )+" a sosit la "+ore+":0"+minute+":"+secunde+". ");
			else if ((minute>=10)&&(secunde<10))
		GUI.rezultat.append("Clientul "+Long.toString( ID )+" a sosit la "+ore+":"+minute+":0"+secunde+". ");
			else
		GUI.rezultat.append("Clientul "+Long.toString( ID )+" a sosit la "+ore+":"+minute+":"+secunde+". ");
          
 
          GUI.rezultat.append("A fost servit in "+tservire/60+" minute si "+ tservire%60 +" secunde");
          GUI.rezultat.append(" la casa "+ Integer.toString(m));
          GUI.rezultat.append("\n");
          casa[ m ].adauga_client( c ); //se adauga la casa cu cei mai putini clienti
          //GUI.rezultat.append("\n");
          
          med= med+tservire; 
          
          sleep( tSosire );
      
          
        } 
    } 
catch ( InterruptedException e )
    { 
        System.out.println( e.toString()); 
    }  
} 
           
public String returnLogString()
{
    return this.logString.toString();
}

}