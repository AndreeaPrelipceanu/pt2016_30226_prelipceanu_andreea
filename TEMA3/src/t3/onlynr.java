package t3;
/**
 * Clasa onlynr  
 * verifica daca sunt doar cifre 
 */
public class onlynr
        {
/**
 * Constructorul onlynr 
 * vreifica string-ul daca afe doar cifre si nu alte caractere
 * return un boolean
 */           
public boolean onlynr(String x)
    {
        if (x == null || x.length() == 0)
                    return false;
        
        for (int i = 0; i < x.length(); i++) {

            //Daca nu este o cifra returneaza false.
            if (!Character.isDigit(x.charAt(i)))
                return false;
        }
        return true;
    }
}