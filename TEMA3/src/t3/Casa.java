package t3;          
import java.util.*;

/**
 * implementeaza case 
 */           
public class Casa extends Thread
{ 
  private Vector clienti=new Vector(); //Vector de clienti
  String t1,t2;
  
/**
 * Metoda Casa  
 * seteaza un numele casei cu ajutorul metodei setName
 */           
public Casa( String name )
{ 
    setName( name );
}

          
public void run()   //porneste firul de executie
    { 
    try{  
        while( true )
        { 
            sleep(Producator.tservire); //se asteapta un timp specific pentru fiecare client	
            sterge_client(); //apoi se sterge clientul
        } 
    } 
    
    catch( InterruptedException e )
    
    {   
        System.out.println("Intrerupere"); 
        System.out.println( e.toString()); //daca apare eroare se tipareste aceasta
    } 
}
          
public synchronized void adauga_client( Client c ) throws InterruptedException 
        {
            clienti.addElement(c); //adauga elementul in vectorul de clienti
            notifyAll(); //invie toate firele aflate in asteptare
        }       
public synchronized void sterge_client() 
    throws InterruptedException 
        { 

          while( clienti.size() == 0 )   //cat timp nu sunt clienti la casa
            wait(); //firul curent e pus in asteptare
            Client c = ( Client ) clienti.elementAt(0); //in c avem elementul din vectorul clienti de pe poz. 0
            clienti.removeElementAt(0);//se elibereaza elementul respectiv 
            sleep(100);
            notifyAll(); //invie toate firele de executie aflate in asteptare
        }
                    
public synchronized long lungime_coada() 
    throws InterruptedException
        { 
          notifyAll(); //invie toate firele ce asteapta evenimente pe acest obiect
          int size = clienti.size(); //se preia si returneaza lungimea vectorului de clienti
          return size; 
        }
}