package t3;
/**
 * Clasa TestStore  
 * implementeaza magazinul
 */           
public class TestStore
{     
/**
 * Constructorul TestStore 
 * creaza fiecare casa pentru magazin
 */           
    public void TestStore( int nrCase, int nrClienti,int tmin,int tmax)
        { 
          int i; 
          Casa c[] = new Casa[ nrCase+1]; 
          for( i=1; i<=nrCase; i++)
            { 
              c[ i ] = new Casa("Casa "+Integer.toString( i )); 
              c[ i ].start(); 
            }
        Producator p = new Producator( nrCase , c, "Producator", nrClienti);
          p.minT=tmin;
          p.maxT=tmax;
          p.start();         
        } 
    public void stop() {};        
}