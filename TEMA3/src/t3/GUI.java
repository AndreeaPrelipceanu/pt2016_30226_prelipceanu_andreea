package t3;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.*;
import java.awt.*;

/**
* Acesta clasa  realizeaza interfata grafica
* a aplicatiei
*/
public class GUI extends JFrame 
     //Declarerea atributelor
    //declararea butoanelor si textfield-urilor folosite
{
     //JLabel item1;

     JTextField tmin;
     JTextField tmax;
     JTextField jnrclienti;
     JTextField jnrcase;
     private boolean ok=false;
     private int time=0;
     
     JLabel smin= new JLabel("Timp minim astepare: ");
     JLabel smax= new JLabel("Timp maxim asteptare: ");
     JLabel nrClienti= new JLabel("Numar clienti: ");
     JLabel nrCase= new JLabel("Numar case: ");

     JPanel wind= new JPanel();
     
     JButton bpornire = new JButton(" START"); 
     TestStore magazin= new TestStore();    
     onlynr test= new onlynr();
     public static JTextArea rezultat=new JTextArea();

    /**
     * Acest constructor adauga textfield-urile si butoanele necesare 
     * si vede cand acestea sunt folosite
     */
    public GUI()
    {
            super("Simulator");

            wind.setLayout(null);
            
            bpornire.setBounds(50,200,180,30);
            wind.add(bpornire);
           
            wind.add(rezultat);
            rezultat.setBounds(40,280,750,400);
            rezultat.setForeground(Color.BLUE);
                                     
            nrClienti.setBounds(40,45,150, 30);
            wind.add(nrClienti);
 
            nrCase.setBounds(40,75,150, 20);
            wind.add(nrCase);

            smin.setBounds(40,105,150, 20);
            wind.add(smin);
        
            smax.setBounds(40,135,150, 20);
            wind.add(smax);
           
        //setarea pozitiilor butoanelor 
        jnrclienti = new JTextField("25",15);
        wind.add(jnrclienti);
        jnrclienti.setBounds(200,45,70,20);
        jnrclienti.setEditable(true); 
        
        jnrcase = new JTextField("3",15);
        wind.add(jnrcase);
        jnrcase.setBounds(200,75,70,20);
        jnrcase.setEditable(true);
        
        tmin= new JTextField("1",15);
        wind.add(tmin);
        tmin.setBounds(200,105,70,20);
        tmin.setEditable(true);
        
        tmax= new JTextField("10",15);
        wind.add(tmax);
        tmax.setBounds(200,135,70,20);
        tmax.setEditable(true);
                
        JLabel a= new JLabel("Introduceti, pe rand, toate datele necesare simularii. "); 
        wind.add(a); 
        a.setBounds(15,5,810,30);
        a.setFont(new Font("Helvetica", Font.BOLD, 15));
        
        JLabel b= new JLabel("Rezultate: "); 
        wind.add(b); 
        b.setBounds(40,240,810,30);
        b.setForeground(Color.RED);
        b.setFont(new Font("Helvetica", Font.BOLD, 15));
        
        JLabel c= new JLabel("* Pentru a putea vizualiza intreaga simulare, numarul recomandat de clienti este 25"); 
        wind.add(c); 
        c.setBounds(320,40,810,30);
        c.setFont(new Font("Helvetica", Font.BOLD, 13));
        
        JLabel d= new JLabel(" "); 
        wind.add(d); 
        d.setBounds(330,60,810,30);
        d.setFont(new Font("Helvetica", Font.BOLD, 13));
        
        JLabel e= new JLabel("** Introduceti cel putin 3 case "); 
        wind.add(e); 
        e.setBounds(320,75,810,30);
        e.setFont(new Font("Helvetica", Font.BOLD, 13));
        
        JLabel f= new JLabel("*** Timpul minim/maxim de asteptare se exprima in minute "); 
        wind.add(f); 
        f.setBounds(320,120,810,30);
        f.setFont(new Font("Helvetica", Font.BOLD, 13));
        
        add(wind);
                 
 class ClickListener implements ActionListener{     
     public void actionPerformed(ActionEvent e)
            {
                String ptmin=" ";
                String ptmax=" ";
                String pnrcl=" ";
                String pnrcz=" ";
                
                String pjmin=" ";
                String pjmax=" ";
                int rtmin, rtmax, rnrcz, rnrcl,rjmin, rjmax;
                
                if(e.getSource()==bpornire)
                       {
                        ptmin=tmin.getText();
                        ptmax=tmax.getText();
                        pnrcl=jnrclienti.getText();
                        pnrcz=jnrcase.getText();

                        if (test.onlynr(ptmin)==false||test.onlynr(ptmax)==false||test.onlynr(pnrcl)==false||test.onlynr(pnrcz)==false)
                        { 
                            rezultat.setText("Eroare!");
                        }
                        else
                        {
                            rnrcz=Integer.parseInt(pnrcz);
                            rnrcl=Integer.parseInt(pnrcl);
                            rtmin=Integer.parseInt(ptmin);
                            rtmax=Integer.parseInt(ptmax);
                            magazin.TestStore(rnrcz, rnrcl, rtmin, rtmax);
               
                        } 
            }  
          }
        }
        bpornire.addActionListener(new ClickListener());
        }
    }
