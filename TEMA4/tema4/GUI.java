/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tema4;


import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/**
* Acesta clasa  realizeaza interfata grafica
* a aplicatiei
*/
public class GUI extends JFrame //implements Runnable{
//Declarerea atributelor
//declararea butoanelor si textfield-urilor folosite
{
     JLabel item1;
     JTextArea rezultat;
     JTextField jnume;
     JTextField jpnume;
     JTextField jiban;
     JTextField jdepun;
     //JTextField jstergecl;
     JTextField jstergecont;
     JTextField jcash;
     JTextField jadresa;
     
    
     private boolean ok=false;
     private int time=0;
     ArrayList<Account> ac = new ArrayList<Account>();
     private static String[] sjbox= {"SELECT","Saving","Spending"};
     
     JLabel nume= new JLabel("NUME:");
     JLabel pnume= new JLabel("PRENUME:");
     JLabel iban= new JLabel("IBAN:");
     JLabel adresa= new JLabel("ADRESA:");
     JLabel depun= new JLabel("SUMA:");
     JLabel jtip= new JLabel("TIP CONT:");
     JPanel wind= new JPanel();
     
     JComboBox tip= new JComboBox(sjbox);
     
     JButton bafiseaza = new JButton("Afiseaza clienti"); 
     JButton badauga = new JButton("Adauga");
     //JButton bsterge = new JButton("Sterge Client");
     JButton bcauta = new JButton("Cauta");
     JButton baddcash = new JButton("Adauga bani");
     JButton bscoate = new JButton("Retrage bani");
     JButton bdelete = new JButton("Sterge Cont");
     //JButton bcauta = new JButton("Cauta");
     // TestStore magazin= new TestStore();    
     
     /**
     * Acest constructor adauga textfield-urile si butoanele necesare 
     * si vede cand acestea sunt folosite
     */
     Bank b=new Bank();
     Client c;
     Account a;
     public GUI()
    {
        super("BANCA");
        //setLayout(new FlowLayout());
            wind.setLayout(null);
            //butoane
            bafiseaza.setBounds(300,10,120,30);
            wind.add(bafiseaza);
                        
            badauga.setBounds(30,270,120,30);
            wind.add(badauga);
                        
           // bsterge.setBounds(460,120,120,30);
           // wind.add(bsterge);
            
            baddcash.setBounds(30,360,120,30);
            wind.add(baddcash);
            
            bscoate.setBounds(30,390,120,30);
            wind.add(bscoate);
            
            bdelete.setBounds(300,120,120,30);
            wind.add(bdelete);
            
            tip.setBounds(130,210,120,30);
            wind.add(tip);
            
             
            jtip.setBounds(30,210,120,30);
            wind.add(jtip);
            
                       
            //textfield-uri           
            rezultat = new JTextArea("");
            
            wind.add(rezultat);
            rezultat.setBounds(270,170,470,500);
                                                
            nume.setBounds(30,50,90,30);
            wind.add(nume);
 
            pnume.setBounds(30,90,90,30);
            wind.add(pnume);

            iban.setBounds(30,10,90,30);
            wind.add(iban);
            
            adresa.setBounds(30,130,90,30);
            wind.add(adresa);
                             
            depun.setBounds(30,170,90,30);
            wind.add(depun);
        //setarea pozitiilor butoanelor 
        
        
        jnume = new JTextField("EU",15);
        wind.add(jnume);
        jnume.setBounds(130,50,110,30);
                
        jpnume = new JTextField("pnume",15);
        wind.add(jpnume);
        jpnume.setBounds(130,90,110,30);
        
        jiban= new JTextField("iban",15);
        wind.add(jiban);
        jiban.setBounds(130,10,110,30);
                
        jadresa= new JTextField("adresa",29);
        wind.add(jadresa);
        jadresa.setBounds(130,130,110,30);
                
        jdepun= new JTextField("depun",15);
        wind.add(jdepun);
        jdepun.setBounds(130,170,110,30);
        
        jcash = new JTextField("cash",15);
        wind.add(jcash);
        jcash.setBounds(35,320,110,30);
        
        /*jstergecl = new JTextField("sterge client",15);
        wind.add(jstergecl);
        jstergecl.setBounds(460,80,110,30);*/
        
        jstergecont = new JTextField("sterge cont",15);
        wind.add(jstergecont);
        jstergecont.setBounds(300,80,110,30);
        
        
        add(wind);
      
        
        
        String[] s;
        Prelucrare i = new Prelucrare();
        
        //initializare clienti
        
    //    s=i.citesteDinFis("clienti.txt");
      //  String[] aux;
     //   for (int j = 1; j < s.length; j++) {
        //    aux = i.transfString(s[j]);
            //if (aux.length > 0) {
             //   b.addClient(new Client(aux[0], aux[1], aux[2], aux[3]));
          //  }
     //   }
        //initializare conturi
        String[] aux;
        s = i.citesteDinFis("account.txt");
        for (int j = 1; j < s.length; j++) {
            aux = i.transfString(s[j]);
            if (aux.length > 0) {
                Account acc;
                Client cc;
                cc=new Client(aux[1],aux[2],aux[3],"CNP");
                if("Spending".equals(aux[5]))
                acc = new SpendingAccount(aux[0], cc,Integer.parseInt(aux[4]));
                else
                acc=new SavingAccount(aux[0], cc,Integer.parseInt(aux[4]),12);
                b.addAccount(c,acc);
                
            }
        }
        
        
        
/**
 * Clasa ClickListener
 */                 
        class ClickListener implements ActionListener
        {
            
/**
 * Metoda actionPerformed
 */           
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String snume;
                String spnume;
                String siban;
                String sadresa;
                String ssuma;
                               
                if(e.getSource()==bafiseaza)
                       {            
                        ac=b.getAccounts();
                        String jj="IBAN"+"\t"+"Nume"+"\t"+"Prenume"+"\t"+"Sold"+"\t"+"Tip\n";
                         for(int j=0;j<ac.size();j++)
                         {
                         jj=jj+ac.get(j).toString();
                         System.out.println("sdfds");
                         }
                         rezultat.setText(jj);
                       b.save("account.txt");
                       }
                
                
                if(e.getSource()==badauga)
                       {
                        snume = jnume.getText();
                        spnume = jpnume.getText();
                        siban = jiban.getText();
                        sadresa = jadresa.getText();
                        ssuma = jdepun.getText();
                        int cassh;
                        cassh = Integer.parseInt(ssuma);
                        
                        c=new Client(snume,spnume,sadresa,"CNP");
                        if("Spending".equals((String)tip.getSelectedItem()))
                               a=new SpendingAccount(siban,c,cassh);
                        else 
                               a=new SavingAccount(siban,c,cassh,12); 
                        
                         b.addAccount(c, a);
                       }
                               
                /* if(e.getSource()==bsterge)
                       {    }  */         
                 
                 if(e.getSource()==bcauta)
                       {            }
                 
                 if(e.getSource()==baddcash)
                       { 
                        ssuma = jcash.getText();
                        int cassh;
                        cassh = Integer.parseInt(ssuma);
                         siban = jiban.getText();
                       ac=b.getAccounts(siban);
                       ac.get(0).deposit(cassh);
                       }
                 
                 if(e.getSource()==bscoate)
                       {
                        ssuma = jcash.getText();
                        int cassh;
                        cassh = Integer.parseInt(ssuma);
                         siban = jiban.getText();
                       ac=b.getAccounts(siban);
                       ac.get(0).withdraw(cassh);
                       }
                 
                 if(e.getSource()==bdelete)
                       { 
                        siban= jstergecont.getText();
                          b.removeAccount(siban);
                       }
                
                 
                           
                       }
        }
        badauga.addActionListener(new ClickListener());
        bafiseaza.addActionListener(new ClickListener());
        //bsterge.addActionListener(new ClickListener());
        bcauta.addActionListener(new ClickListener());
        baddcash.addActionListener(new ClickListener());
        bscoate.addActionListener(new ClickListener());
        bdelete.addActionListener(new ClickListener());
      
                
                
                       // ptmin=tmin.getText();
                       /* ptmax=tmax.getText();
                        pnrcl=jnrclienti.getText();
                        pnrcz=jnrcozi.getText();*/
                      //  if (test.onlynr(ptmin)==false||test.onlynr(ptmax)==false||test.onlynr(pnrcl)==false||test.onlynr(pnrcz)==false)
                      //  { rezultat.setText("Eroare");
                     //   }
                    //    else
                    //    {rnrcz=Integer.parseInt(pnrcz);
                     //    rnrcl=Integer.parseInt(pnrcl);
                     //    rtmin=Integer.parseInt(ptmin);
     
             //            rtmax=Integer.parseInt(ptmax);
                           // magazin.TestStore(rnrcz, rnrcl, rtmin, rtmax);
                            //Producator prod= new Producator(rnrcz,);
                            //rezultat.append(prod.returnLogString());
    }
}
                        
                        
             
//        bpornire.addActionListener(new ClickListener());*/
    
    
    