package tema4;

public class Account {
	protected String number;
	protected Client client;
	protected double amount;

	public Account() {}
	/**
         * Metoda getAccount() stie numarul contului
         * @return - numarul contului
         */
        public String getAccount(){
		return number;
	}
        
        /**
         * Constructorul Account 
         * @param number -numar de Cont
         * @param client -Clientul caruia ii apartine contul
         * @param solde  -sold
         */
	public Account(String number, Client client, double solde) {
		this.number = number;
		this.client = client;
		this.amount = solde;
	}

        /**
         * Metoda transfer() transfera bani dintr-un cont in altul
         * @param c -Cont din care se scot bani
         * @param amt -Suma care trebuie scoasa din cont
         */
        public void transfer(Account c, int amt){
		if(amt>amount)
			System.out.println("insuficienti bani");
		else{		
			withdraw(amt);
			c.deposit(amt);
		}

	}
        /**
         * Metoda deposit() depune bani in cont
         * @param amt -Suma care trebuie depusa in cont
         */
	public void deposit(int amt)
	{
		this.amount += amt;

	}
        
         /**
         * Metoda withdraw() retrage bani in cont
         * @param amt -Suma care trebuie retrasa in cont
         */
	public void withdraw(int amt) 
	{
		amount=amount-amt;
	}
        /**
         * Metoda whoAreYou() retine tipul contului
         * @return -tipul contului
         */
        public String whoAreYou()
	{ return whoAreYou(); }
         /**
         * Metoda toString() creaza un string datele contului
         * @return - string-ul cu date
         */
 public String toString(){
	 return number+"\t"+client.toString()+"\t"+amount+"\t"+whoAreYou()+"\n";
 } 
}



