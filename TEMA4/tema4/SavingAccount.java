package tema4;


public class SavingAccount extends Account {
    protected int perioada;//definita in luni
    protected static float dobanda;
    SavingAccount(){
	super ();
	}
  
 public SavingAccount(String id,Client p,double suma,int per)
      {
	super(id,p,suma);
	this.perioada = per;
	dobanda=(float) 0.05;
	
	}
	
  public float calculeazaRata()
	{
	//rata este de rata% iar cu fiecare luna in plus creste cu cate rata/10%
   	return (float) (dobanda*this.amount+(perioada-1)*dobanda*this.amount/10);
	}
	/**
         * Metoda adaugaSuma - retrage bani din cont
         * @param s - valoarea banilor ce vor fi retrasi
         */
 public boolean retrageBani(float s)
	{
	  if(amount>=s) { amount=amount-s; 
                          return true;}
	  return false;
	}
	/**
         * Metoda adaugaSuma - depune bani in cont
         * @param a - valoarea banilor depusi
         */
 public void adaugaSuma(float a)
	{
		this.amount=this.amount+a;
	}
	
	
	
public void schimaPerioada(int per)
	{
		perioada=per;
	}
public void cumuleazaDobanda()
	{
		this.amount=this.amount+this.calculeazaRata();
	}

         /**
         * Metoda whoAreYou - returneaza tipul contului
         * @return tipul "Saving"
         */
public String whoAreYou()
{ return "Saving"; }

         /**
         * Metoda getRata - returneaza dobanda contului
         * @return dobanda
         */
public float getRata()
	{return dobanda;}

         /**
         * Metoda setRata - seteaza noua dobanda a contului contului
         * @param a - valoarea noii dobanzi
         */
public void setRata(float a)
	{dobanda=a;}
         /**
         * Metoda getPerioada - returneaza perioada
         * @return perioada
         */
public int getPerioada()
	{return perioada;}
}

   
