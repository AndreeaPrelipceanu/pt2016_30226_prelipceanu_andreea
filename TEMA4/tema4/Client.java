package tema4;

public class Client {
	protected String lastName;
	protected String firstName;
	protected String address;
        protected String cnp;
	protected String id;

	public Client() {
	}
	public Client (String id){
		this.id=id;
	}
        
         /**
     * Constructorul clasei Client
     * @param lastName - prenume Client
     * @param firstName - nume Client
     * @param address - adresa Client
     * @param cnp -CNP Client
     */
	public Client(String lastName,String firstName,String address, String cnp) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.address = address;
                this.cnp=cnp;
	}
        
         /**
     * Metoda toString - returneaza un string 
     * @return String - format din nume si prenume
     */
public String toString(){
	return firstName+"\t"+lastName;
}

}

