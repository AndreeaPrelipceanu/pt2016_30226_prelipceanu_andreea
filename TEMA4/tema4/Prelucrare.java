/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tema4;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Prelucrare {
    
    /**Aceasta metoda citeste datele dintr-un fisier specificat.
     * @param fis Un sir de caractere ce reprezinta numele fisierului din 
     * care se face citirea.
     * @return Un sir de siruri de carcatere ce contin ficare line din fisier.
     */
    public String[] citesteDinFis(String fis) {
        String[] s;
        FileReader reader;
        try {
            reader = new FileReader(fis);
            Scanner in = new Scanner(reader);
            s = new String[in.nextInt() + 1];
            for (int i = 0; i < s.length; i++) {
                s[i] = in.nextLine();
            }
            reader.close();
            return s;

        } catch (FileNotFoundException e) {
            System.out.println("nu s-a gasi fis");
        } catch (IOException e) {
        }
        s = new String[0];
        return s;
    }

    /**Aceasta metoda extrage toate cuvinetele dintr-un sir de carcatere.
     * 
     * @param line Un sir de caractere ce contine mai multe cuvinte.
     * @return Un sir de siruiri de caractere fiecare cu cate un cuvant extras din sirul initial.
     */
    public String[] transfString(String line) {
        String[] s;
        int i = 0;
        StringTokenizer st = new StringTokenizer(line, " \t");
        try {
            s = new String[st.countTokens()];
            while (st.hasMoreElements()) {
                s[i] = st.nextToken();
                i++;
            }
        } catch (NoSuchElementException e) {
            s = new String[0];
        }
        return s;
    }
}

    
  