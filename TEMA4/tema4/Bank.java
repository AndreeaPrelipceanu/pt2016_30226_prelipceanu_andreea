package tema4;

import java.io.*;
import java.util.*;

public class Bank implements BankProc{
  private HashMap<String,ArrayList<Account>> accounts;
  public Bank() {
	accounts = new HashMap<String, ArrayList<Account>>();
	}

	/**
	 * Adauga un cont unui client. 
	 * @param c - client
	 * @param account - contul adaugat clientului
         */
 public void addAccount(Client c, Account account)
         
	{assert account!=null;
         int sizePre= accounts.size();
            String iban=account.number;
             ArrayList<Account> values = accounts.get(iban);
		if (values == null) {
			values = new ArrayList<Account>();
		}
		if (!values.contains(account)) {
			values.add(account);
			accounts.put(iban, values);
		}
            int sizePost= accounts.size();
            assert sizePost== sizePre+ 1;
	}

	/**
	 * Gets all the accounts for a client
	 * @param cnp
	 * @return an array of accounts
	 */
   public  ArrayList<Account> getAccounts(String iban) 
   {
       assert accounts.containsKey(iban)!=true && accounts.isEmpty()!=true;
		return accounts.get(iban);
	}
   
   public  ArrayList<Account> getAccounts()
        {
            assert accounts.isEmpty()!=true;
          
            ArrayList<Account> ac=new ArrayList<Account>();
            Collection 	c;
            c=accounts.values();
            Iterator i;
            i=c.iterator();
            while(i.hasNext())
            {
                Account a1;
                a1=((ArrayList<Account>)i.next()).get(0);
                    ac.add(a1);
            }
            return ac;
	}
/**
          * Salveaza in fisier conturi
          * @pre fileName!=""; 
          * @param fileName -fisierul in care se salveaza
          * @post nochange  
          */
    @Override
	public void save(String fileName){
		try{
			assert !"".equals(fileName);
                                             ArrayList<Account> acc= new ArrayList<Account>();
			BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
                        out.write(""+accounts.size());
                        out.newLine();
                        acc=getAccounts();
			for(int j=0;j<acc.size();j++)
                        {
				out.write(""+acc.get(j).number+" "+acc.get(j).client.firstName+" "+acc.get(j).client.lastName+" "+acc.get(j).client.address+" "+(int)acc.get(j).amount+" "+acc.get(j).whoAreYou());
				out.newLine();
			}
                       out.close();
		}
		catch (IOException e) {}
		}
    
		
/**
 * @pre key!=null
 * @param key
 * @post nochange
 * @return s
 */
	public String listToString(String key){
		assert key!=null;
		String s="";
		Iterator i=accounts.get(key).iterator();
		while(i.hasNext()){
			s=s+i.next().toString();
		}
		return s;
	}

		
public void search(String key){
		
		Enumeration en = (Enumeration) accounts.keySet();
		ArrayList<Account> myList =new ArrayList<Account>();
		while(en.hasMoreElements()){
			String key1 = (String) en.nextElement();
		 	if (key.equals(key1))
		 				myList = accounts.get(key);
		}
		 	for(Account acc : myList)
			 System.out.println(acc.toString());
			
	}
		


	/**
	 * Metoda removeAccount - sterge un cont din lista de conturi
	 * @param iban - codul unic al contului dupa care se sterge
	 */
	public void removeAccount(String iban) {
		assert (accounts != null && accounts.get(iban) != null);
		 int sizePre= accounts.size();
		
                accounts.remove(iban);
                               
                int sizePost= accounts.size();
                assert sizePost== sizePre- 1;

	}

}

