/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tema4;


public class SpendingAccount extends Account
{
	protected static float rata;
        
        /**
         * 
         * @param id - IBAN al contului
         * @param p - clientul contului
         * @param suma -suma din cont
         */
	
	public SpendingAccount(String id,Client p,double suma)
	{
	super(id,p,suma);
	rata=(float) 0.06; 
	}
	
	public boolean retrageBani(float s)
	{
		return false; //nu se mai pot retage bani dintr-un cont de rate
	}
	
	public void cumuleazaDobanda()
	{}
	public void schimaPerioada(int per){}
	
        /**
         * Metoda whoAreYou - returneaza tipul contului
         * @return tipul "Spending"
         */
	public String whoAreYou()
	{ return "Spending"; }
        
     /**
     * Calculeaza rata
     * @return - rata este de rata% iar cu fiecare luna in plus creste cu cate rata/5%
     */
	
	public float calculeazaRata()
	{
		//rata este de rata% iar cu fiecare luna in plus creste cu cate rata/5%
		return (float) (rata*this.amount+rata*this.amount/5);
	}
	
        
	public void adaugaSuma(float a)
	{
		this.amount=this.amount-a;  //scadem din suma care mai trebuie returnata bancii
	}
	
        /**
         * Metoda getRata - returneaza rata contului
         * @return returneaza rata contului
         */
	public float getRata()
	{return rata;}
        
        /**
         * Metoda setRata - seteaza rata contului
         * @param a noua rata a contului
         */
	public void setRata(float a)
	{rata=a;}
	public int getPerioada()
	{return 0;}
}

