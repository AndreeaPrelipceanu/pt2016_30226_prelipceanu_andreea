package tema4;

import java.util.ArrayList;

public interface BankProc {
                
                 /**
                 * Adauga un cont unui client
                 * @pre c!=null
                 * @pre account!=null
                 * @param c -clientul
                 * @param account -contul atribuit
                 * @post getSize() == getSize()@pre+ 1
                 */
		public void addAccount(Client c, Account account);
                 
                /**
                 * Returneaza toate conturile unui Client dupa CNP
                 * @param cnp - CNP clientului pentru care se vor afisa conturile
                 * @return - un array de conturi
                 */
		public  ArrayList<Account> getAccounts(String cnp);

		

		public void search(String key);
		
                
                 /**
                 * Salveaza in fisier conturi
                 * @param fileName -fisierul in care se salveaza
                */
		public void save(String fileName);

		
                 /**
                 * Sterge un cont asociat unui client
                 * @param cnp -- CNP clientului pentru care se va sterge contul
                 * @param account -- contul ce va fi sters
                 */    
		public void removeAccount(String cnp);

	}
