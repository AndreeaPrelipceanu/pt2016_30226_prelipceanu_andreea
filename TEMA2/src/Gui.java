import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class Gui implements ActionListener{

	/*Input Data*/
	/////////////////////////////////////////////////////
	private JTextField name = new JTextField(10);
	private JTextField prod_code = new JTextField(10);
	private JTextField customer  = new JTextField(10);
	private JTextField quantity = new JTextField(3);
	private JTextField price = new JTextField(3);
	private JTextField selQuantity = new JTextField(3);
	//////////////////////////////////////////////////////
	
	
	/*Input Labels*/
	/////////////////////////////////////////////////////
	private JLabel nameLabel = new JLabel("* Nume: ");
	private JLabel prod_codeLabel = new JLabel("* Product Code: ");
	private JLabel customerLabel  = new JLabel("Customer: ");
	private JLabel quantityLabel = new JLabel("Available Quantity: ");
	private JLabel priceLabel = new JLabel("Price: ");
	private JLabel selQuantityLabel = new JLabel("Quantity: ");
	private JLabel instr = new JLabel("Only the fields with * are used for search");
	//////////////////////////////////////////////////////
	
	/*Radio Buttons*/
    JRadioButton searchButton = new JRadioButton("Search",false);    
    JRadioButton enterButton = new JRadioButton("Enter",true);
    ButtonGroup group = new ButtonGroup();
    
    JButton sell = new JButton("SELL");
    JButton go = new JButton("GO");
           
    /*Output*/
    private JTextArea textArea = new JTextArea(5,20);
    
	/*window*/
	////////////////////////////////////////////////////////
	private	JFrame frame = new JFrame();
	private JPanel content = new JPanel(new GridLayout(2,1));
	private JPanel inputGrid = new JPanel(new GridLayout(6,4));
	private JPanel outputFields = new JPanel(new BorderLayout());
	////////////////////////////////////////////////////////
	
	/*Window Dimension*/
	Dimension d = new Dimension(200,400);

    
    public Gui()
    {    	
		/*add Action Listener*/
		////////////////////////////////////////////////////////
		searchButton.addActionListener(this);
		enterButton.addActionListener(this);
		sell.addActionListener(this);
		go.addActionListener(this);
		
    	searchButton.setActionCommand("0");
    	enterButton.setActionCommand("1");
    	go.setActionCommand("2");
    	sell.setActionCommand("3");
		////////////////////////////////////////////////////////
    	
    	//group the radio buttons
    	group.add(searchButton);
        group.add(enterButton);
        
        inputGrid.add(searchButton);     inputGrid.add(enterButton);
        inputGrid.add(new JLabel(""));   inputGrid.add(new JLabel(""));
        inputGrid.add(nameLabel);		 inputGrid.add(name);
        inputGrid.add(prod_codeLabel);	 inputGrid.add(prod_code);
        inputGrid.add(quantityLabel);	 inputGrid.add(quantity);
        inputGrid.add(priceLabel);		 inputGrid.add(price);
                                         inputGrid.add(new JLabel(""));
        inputGrid.add(customerLabel);	 inputGrid.add(customer);
        inputGrid.add(new JLabel(""));   inputGrid.add(new JLabel(""));
        inputGrid.add(go);				 inputGrid.add(new JLabel(""));
        inputGrid.add(new JLabel(""));	 inputGrid.add(new JLabel(""));
        inputGrid.add(selQuantityLabel); inputGrid.add(selQuantity);
        inputGrid.add(sell);        
        
        
        JScrollPane scrollPane = new JScrollPane(textArea); 
		textArea.setEditable(false);
     	customer.setEditable(false);

                
        outputFields.add(instr,BorderLayout.NORTH);
        outputFields.add(scrollPane,BorderLayout.CENTER);
        
        content.add(inputGrid);
        content.add(outputFields);
        
        /* Set Frame */
		////////////////////////////////////////////////////////
		frame.setContentPane(content);
		frame.setTitle("Order Management");
		//frame.setSize(d);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		////////////////////////////////////////////////////////
    }
    
    public Order parseInputData(int way)
    {
    	String pname = name.getText(); String cname = customer.getText();
    	int pi=0; int q=0; int p=0; int oq=0;
    	try{
         pi = Integer.parseInt(prod_code.getText());
    	 q = Integer.parseInt(quantity.getText());
    	 p = Integer.parseInt(price.getText());
    	 oq = Integer.parseInt(selQuantity.getText());
    	}
    	catch(NumberFormatException e)
    	{
    		return null;
    	}
    	Product pr = new Product();
    	pr.setName(pname);
		pr.setPid(pi);
    	pr.setPrice(p);
    	if(way==0)
    	pr.setQuantity(q+oq);
    	else if(way==1)
    		pr.setQuantity(q-oq);
    	
    	Customer cu = new Customer();
    	if(way==0)
    	cu.setName("Provider");
    	else if(way==1)
        	cu.setName(cname);    		
    	
    	Order o = new Order();    	
    	o.setCustomer(cu);
    	o.setWay(0);
    	o.setProduct(pr);
    	
    	return o;
    	
    	
    }
    
	@Override
	public void actionPerformed(ActionEvent e) {
		
		int cmd = Integer.parseInt(e.getActionCommand());

		switch(cmd)
		{
		case 0: //case search selected
			
			 prod_code.setEditable(false);
			 customer.setEditable(true);
			 quantity.setEditable(false);
			 price.setEditable(false);
			 
			break;
		case 1:
			 prod_code.setEditable(true);
			 customer.setEditable(false);
			 quantity.setEditable(true);
			 price.setEditable(true);
			 
			break;
		case 2:
			if(searchButton.isSelected())
			{
				//do the search
				Warehouse w = new Warehouse();
				w.parseDocument();
				String prodName = name.getText();
				if(prodName.equals("*"))
				{
					w.t.inorderBST();
					BNode[] nodes = w.t.getNodes();
					int i=0;
					while(nodes[i]!=null)
					{
						textArea.append(" Product Name "+nodes[i].prod.getName()+ " || " +
										" Quantity " + nodes[i].prod.getQuantity()+ " || " + 
										" Price "+ nodes[i].prod.getPrice()+ " lei " + " || " +
										" PID " + nodes[i].prod.getPid()+  "\n ");
						i++;
					}
					
				}
				else{
					Product prod = w.t.searchProductBST(prodName);
					if (prod!=null){
						prod_code.setText(""+prod.getPid());
						quantity.setText(""+prod.getQuantity());
						price.setText(""+prod.getPrice());
						textArea.append("Produsul "+ prod.getName()+" exista in baza de date \n ");
					}
					else
						textArea.append("Produsul "+ prodName+" nu exista in baza de date \n ");
				}
				
			}
			else if(enterButton.isSelected())
			{
				//do the insertion
				
				Order ord = parseInputData(0);
				if(ord!=null){
				OPDept process = new OPDept();
				process.parseDocument();
				ord.setOid(process.getOId()+1);
				process.addOrder(ord);
				
				}
				else textArea.append("Cannot process order");
				
			}
			break;
		case 3:
			if(searchButton.isSelected())
			{
				//do the sell
				Order ord = parseInputData(1);
				if(ord!=null){
				OPDept process = new OPDept();
				process.parseDocument();
				ord.setOid(process.getOId()+1);
				process.addOrder(ord);
				}
				else textArea.append("Cannot process order");
			}
			break;
				
		}
		
	
	}
	
	

}
