
public class Customer {
	
	String name;
	String cid;
	
	public Customer() {
		this.name="";
		this.cid="";
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
