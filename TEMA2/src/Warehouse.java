import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.*;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Warehouse extends DefaultHandler{

	Product tempProd;
	String tempVal;
	BinTree t = new BinTree(1);
    
  
	
	public void parseDocument() {

		//get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			//get a new instance of parser
			SAXParser sp = spf.newSAXParser();
			//parse the file and also register this class for call backs
			sp.parse("warehouse.xml",this);


		}catch(SAXException se) {
			se.printStackTrace();
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	//Event Handlers
		public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
			
			//reset
			tempVal = "";
			if(qName.equalsIgnoreCase("Product")) {
				//create a new instance of employee
				tempProd = new Product();
			}
		}


		public void characters(char[] ch, int start, int length) throws SAXException {
			tempVal = new String(ch,start,length);

		}

		public void endElement(String uri, String localName,
			String qName) throws SAXException {

			if(qName.equalsIgnoreCase("Product")) {
				//System.out.println("Nume "+tempProd.getName()+" PID "+ tempProd.getPid()+" Price "+tempProd.getPrice()+" Quantity "+tempProd.getQuantity());
				//add it to the list
				t.insertBST(tempProd);
			}else if (qName.equalsIgnoreCase("Name")) {
				tempProd.setName(tempVal);
			}else if (qName.equalsIgnoreCase("Id")) {
				tempProd.setPid(Integer.parseInt(tempVal));
			}else if (qName.equalsIgnoreCase("Price")) {
				tempProd.setPrice(Integer.parseInt(tempVal));
			}else if (qName.equalsIgnoreCase("Quantity")) {
				tempProd.setQuantity(Integer.parseInt(tempVal));
			}

		}
		
		public void printData()
		{
			t.inorderBST();
		}
		
	
	
		public void write() throws FileNotFoundException
		{
			t.inorderBST();
			BNode[] nodes = t.getNodes();
			
			OutputStream outputStream = new FileOutputStream(new File("warehouse.xml"));

			XMLStreamWriter out;
			try {
				out = XMLOutputFactory.newInstance().createXMLStreamWriter(
				                new OutputStreamWriter(outputStream, "utf-8"));
				
				out.writeStartDocument();
				out.writeStartElement("warehouse");
				
			int i=0;
			while(nodes[i]!=null)
			{		
				out.writeStartElement("product");	
	
				
			out.writeStartElement("Name");
			out.writeCharacters(nodes[i].prod.getName());
			out.writeEndElement();
			
			out.writeStartElement("Id");
			out.writeCharacters(""+nodes[i].prod.getPid());
			out.writeEndElement();
			
			out.writeStartElement("Price");
			out.writeCharacters(""+nodes[i].prod.getPrice());
			out.writeEndElement();
			
			out.writeStartElement("Quantity");
			out.writeCharacters(""+nodes[i].prod.getQuantity());
			out.writeEndElement();
			
			out.writeEndElement();

			i++;
		        }

			out.writeEndElement();
			out.writeEndDocument();

			out.close();
			} 
			catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		

	
}
