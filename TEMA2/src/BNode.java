public class BNode implements Factory{

    public BNode leftBNode,  rightBNode; 
    public Product prod; 
    public Order ord;
    public int type=3;
    
    public BNode createNode(Product prod) {
        this.prod= prod;
        this.leftBNode = null;
        this.rightBNode = null;
        this.type=1;
        return this;
    }
    
    public BNode createNode(Order ord) {
        this.ord= ord;
        this.leftBNode = null;
        this.rightBNode = null;
        this.type=0;
        return this;
    }

	@Override
	public BNode setNode(int type) {

		if(type==0) //order node
			return createNode(ord);
		else if(type==1) //product node
			return createNode(prod);
		else return this;
	}
	
	public int getType()
	{
		return this.type;
	}

	public void show() {
		if(this.getType()==0)
			ord.print();
		else if(this.getType()==1)
			prod.print();
	}
}