
public class BinTree {
    BNode theBTRootNode = new BNode();
    BNode[] nodes = new BNode[200];
    int i=0;
	private Product prod;

    public BinTree(int type) // constructor
    {
    	theBTRootNode=theBTRootNode.setNode(type);
        theBTRootNode = null;
    }

    // ------------------ Addition of the node to the BST-------------------
    protected BNode insert(BNode theRootNode, BNode myNewNode) {
        if (theRootNode == null) {
            theRootNode = myNewNode;
	        } else if(myNewNode.getType()==0)      	
	    {
	        	if (myNewNode.ord.oid < theRootNode.ord.oid) {
	            theRootNode.leftBNode = insert(theRootNode.leftBNode, myNewNode);
	        } else if (myNewNode.ord.oid > theRootNode.ord.oid) {
	            // else if bigger appends to the right
	            theRootNode.rightBNode = 
	               insert(theRootNode.rightBNode, myNewNode);
	        }
        }
        else
        	 if(myNewNode.getType()==1)      	
             {
	             	if (myNewNode.prod.getName().compareToIgnoreCase(theRootNode.prod.getName())<0) {
	                 theRootNode.leftBNode = insert(theRootNode.leftBNode, myNewNode);
	             } else if(myNewNode.prod.getName().compareToIgnoreCase(theRootNode.prod.getName())>0){
	                 // else if bigger appends to the right
	                 theRootNode.rightBNode = 
	                    insert(theRootNode.rightBNode, myNewNode);
	             }
	             	else if(myNewNode.prod.getName().compareToIgnoreCase(theRootNode.prod.getName())==0)
	             	{
	             		//theRootNode.prod.setPrice(theRootNode.prod.getPrice()+myNewNode.prod.getPrice());
	             		theRootNode.prod.setQuantity(myNewNode.prod.getQuantity());
	             	}
             }
        return theRootNode;
    }
    
   
    
    

    public void insertBST(Product prod) {
        this.prod = prod;
		BNode productBTNode = new BNode();
        productBTNode=productBTNode.setNode(1);
        productBTNode.prod=prod;
         //calls insert above
        theBTRootNode = insert(theBTRootNode, productBTNode);
    }
    
    public void insertBST(Order ord) {
        BNode orderBTNode = new BNode();
        orderBTNode=orderBTNode.setNode(0);
        orderBTNode.ord=ord;
         //calls insert above
        theBTRootNode = insert(theBTRootNode, orderBTNode);
    }
    

    // ------------------ InOrder traversal------Product prod-------------
    protected void inorder(BNode theRootNode) {
        if (theRootNode != null) {        	
            inorder(theRootNode.leftBNode);  
            theRootNode.show();
            nodes[i]=theRootNode; i++;
            inorder(theRootNode.rightBNode);            
        }
    }

    //calls the method to do in order
    public void inorderBST() {
        inorder(theBTRootNode);
    }
    
    public BNode[] getNodes()
    {
    	return this.nodes;
    }

    // ----- Search for key name and  returns ref. 
    //              to BNode or null if not found--------
    protected BNode search(BNode theRootNode, String key) {
        //if the root is null returns null
        if (theRootNode == null) {
            return null;
        } else {
        	if(theRootNode.getType()==0){
	            //checks if they are equal
	            if (Integer.parseInt(key)==theRootNode.ord.oid) {
	                return theRootNode;
	            //checks id the key is smaller than the current
	            //record  if smaller traverses to the left
	            } else if (Integer.parseInt(key)<theRootNode.ord.oid) {
	                return search(theRootNode.leftBNode, key);
	            } else {
	                // if bigger traverses to the left
	                return search(theRootNode.rightBNode, key);
	            }
        	}
        	else
        	{
        		if(theRootNode.getType()==1){
    	            //checks if they are equal
    	            if (key.compareToIgnoreCase(theRootNode.prod.name) == 0) {
    	                return theRootNode;
    	            //checks id the key is smaller than the current
    	            //record  if smaller traverses to the left
    	            } else if (key.compareToIgnoreCase(theRootNode.prod.name) < 0) {
    	                return search(theRootNode.leftBNode, key);
    	            } else {
    	                // if bigger traverses to the left
    	                return search(theRootNode.rightBNode, key);
    	            }
            	}
        	}
            }
		return theRootNode; /////////?????
    }

    //returns null if no result else returns 
    //the order object matched with the keyName
    public Order searchOrderBST(String keyName) {
        BNode temp = search(theBTRootNode, keyName);
        if (temp == null) {
      //noresults found
           return null;
        } else {
         //result found
           return temp.ord;
        }
    }
    
    //returns null if no result else returns 
    //the product object matched with the keyName
    public Product searchProductBST(String keyName) {
        BNode temp = search(theBTRootNode, keyName);
        if (temp == null) {
      //noresults found
           return null;
        } else {
         //result found
           return temp.prod;
        }
    }
}