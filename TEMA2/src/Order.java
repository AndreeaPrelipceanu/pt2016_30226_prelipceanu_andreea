
public class Order {
	
	int oid; //order id
	int quantity;
	Product product;
	Customer customer;
	int way; //0 product in , 1 product out
	
	public Order() {
		this.oid = 0;
		this.quantity=0;
		this.product = new Product();
		this.customer = new Customer();
		this.way = 3;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getOid() {
		return oid;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getWay() {
		return way;
	}

	public void setWay(int way) {
		this.way = way;
	}

	public void print() {
		System.out.println("Ord"+this.getOid()+" Customer "+this.getCustomer().getName()+" Product "+ this.getProduct().getName());
	}	

}
