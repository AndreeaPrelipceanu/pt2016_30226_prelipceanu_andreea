
public class Product {
	
	String name;
	int pid;
	int quantity;
	int price;
	


	public Product() {
		this.name = "";
		this.quantity = 0;
		this.price=0;
	}

	
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void print() {
		System.out.println("Product "+this.getName());
	}
	
	
	

}
