import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;

public class OPDept extends DefaultHandler{

	Order tempOrd;
	String tempVal;
	BinTree t = new BinTree(0);
	
	
	public void parseDocument() {

		//get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			//get a new instance of parser
			SAXParser sp = spf.newSAXParser();
			//parse the file and also register this class for call backs
			sp.parse("orders.xml",this);


		}catch(SAXException se) {
			se.printStackTrace();
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	//Event Handlers
		public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
			//reset
			tempVal = "";
			if(qName.equalsIgnoreCase("Order")) {
				//create a new instance of employee
				tempOrd = new Order();
			}
		}


		public void characters(char[] ch, int start, int length) throws SAXException {
			tempVal = new String(ch,start,length);

		}

		public void endElement(String uri, String localName,
			String qName) throws SAXException {

			if(qName.equalsIgnoreCase("Order")) {
				//System.out.println("Nume "+tempProd.getName()+" PID "+ tempProd.getPid()+" Price "+tempProd.getPrice()+" Quantity "+tempProd.getQuantity());
				//add it to the list
				t.insertBST(tempOrd);

			}else if (qName.equalsIgnoreCase("Oid")) {
				tempOrd.setOid(Integer.parseInt(tempVal));
			}else if (qName.equalsIgnoreCase("Oqnty")) {
				tempOrd.setQuantity(Integer.parseInt(tempVal));
			}
			else if (qName.equalsIgnoreCase("Name")) {
				tempOrd.getProduct().setName(tempVal);
			}else if (qName.equalsIgnoreCase("Id")) {
				tempOrd.getProduct().setPid(Integer.parseInt(tempVal));
			}else if (qName.equalsIgnoreCase("Price")) {
				tempOrd.getProduct().setPrice(Integer.parseInt(tempVal));
			}else if (qName.equalsIgnoreCase("Quantity")) {
				tempOrd.getProduct().setQuantity(Integer.parseInt(tempVal));
			}
			else if (qName.equalsIgnoreCase("CName")) {
				tempOrd.getCustomer().setName(tempVal);
			}else if (qName.equalsIgnoreCase("Way")) {
				tempOrd.setWay(Integer.parseInt(tempVal));
			}

		}
		
		
		
		public void printData()
		{
			t.inorderBST();
		}
		
		
		public int getOId()
	    {
	    	t.inorderBST();
	    	BNode[] nodes = t.getNodes();
	    	int i=0;
			while(nodes[i]!=null)
			{
				i++;
			}
			
	    	return nodes[i-1].ord.getOid();
	    }
		
		public void addOrder(Order o)
		{
			
//			if(o.getWay()==1) //product out
//				o.getProduct().setQuantity(0-o.getProduct().getQuantity());
			
			t.insertBST(o);
			updateWarehouse(o.getProduct());	
			try {
				write();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
		public void updateWarehouse(Product prod)
		{
			Warehouse w = new Warehouse();
			w.parseDocument();
			w.t.insertBST(prod);
			try {
				w.write();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	
		public void write() throws FileNotFoundException
		{
			t.inorderBST();
			BNode[] nodes = t.getNodes();
			
			OutputStream outputStream = new FileOutputStream(new File("orders.xml"));

			XMLStreamWriter out;
			try {
				out = XMLOutputFactory.newInstance().createXMLStreamWriter(
				                new OutputStreamWriter(outputStream, "utf-8"));
				
				out.writeStartDocument();
				out.writeStartElement("orders");
				
			int i=0;
			while(nodes[i]!=null)
			{		
			out.writeStartElement("order");	
	
				out.writeStartElement("Oid");
				out.writeCharacters(""+nodes[i].ord.getOid());
				out.writeEndElement();
				
				out.writeStartElement("Oqnty");
				out.writeCharacters(""+nodes[i].ord.getQuantity());
				out.writeEndElement();
				
				out.writeStartElement("product");	
					out.writeStartElement("Name");
					out.writeCharacters(nodes[i].ord.getProduct().getName());
					out.writeEndElement();
					
					out.writeStartElement("Id");
					out.writeCharacters(""+nodes[i].ord.getProduct().getPid());
					out.writeEndElement();
					
					out.writeStartElement("Price");
					out.writeCharacters(""+nodes[i].ord.getProduct().getPrice());
					out.writeEndElement();
					
					out.writeStartElement("Quantity");
					out.writeCharacters(""+nodes[i].ord.getProduct().getQuantity());
					out.writeEndElement();
				out.writeEndElement();
				
				out.writeStartElement("customer");	
					out.writeStartElement("CName");
					out.writeCharacters(nodes[i].ord.getCustomer().getName());
					out.writeEndElement();
				out.writeEndElement();

				out.writeStartElement("Way");
				out.writeCharacters(""+nodes[i].ord.getWay());
				out.writeEndElement();
			out.writeEndElement();

			i++;
		        }

			out.writeEndElement();
			out.writeEndDocument();

			out.close();
			} 
			catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FactoryConfigurationError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		
	
}
